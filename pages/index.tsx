import React from 'react';
import dynamic from 'next/dynamic';

import Head from '../src/components/Head';
import FriendsStore from '../src/stores/FriendsStore';
import { FriendsStoreContext } from '../src/context';

const FriendsMapWrapper = dynamic(
  () => import('../src/components/FriendsMapWrapper'),
  {
    ssr: false,
  },
);

const Home = () => (
  <FriendsStoreContext.Provider value={FriendsStore.getInstance()}>
    <Head title="Home" />

    <FriendsMapWrapper />
  </FriendsStoreContext.Provider>
);

export default Home;
