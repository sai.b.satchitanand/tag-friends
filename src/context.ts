import React, { useContext } from 'react';
import FriendsStore from './stores/FriendsStore';

export const FriendsStoreContext = React.createContext<FriendsStore>(
  FriendsStore.getInstance(),
);

export const useFriendsStoreContext = () => useContext(FriendsStoreContext);
