import Ajv from 'ajv';
import Schema from '../../generated/schema.json';

const ajv = new Ajv();
ajv.addSchema(Schema, 'app');

export default ajv;
