export default function isServer(): boolean {
  return !(process && (process as any).browser);
}
