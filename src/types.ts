// json schema will be auto generated for the types in this file
import { LatLng } from 'leaflet';

export interface Friend {
  name: string;
  age?: number;
  photo: string;
  position: LatLng;
}

export type Friends = Friend[];
