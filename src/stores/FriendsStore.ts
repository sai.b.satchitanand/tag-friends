import { observable, action, computed, toJS } from 'mobx';

import schemaChecker from '../utils/ajv';
import isServer from '../utils/isServer';
import { Friend, Friends } from '../types';

export {
  Friend,
};

export default class FriendsStore {
  private static instance: FriendsStore;
  @observable friends: Friends = [];
  @observable selectedFriendIdx: number;

  static getInstance() {
    if (isServer() || this.instance === undefined) {
      return this.instance = new this();
    }
    return this.instance;
  }

  @computed
  get friendsToString() {
    if (!this.friends.length) {
      return undefined;
    }
    return JSON.stringify(toJS(this.friends), null, 2);
  }

  @computed
  get selectedFriendPosition() {
    if (this.selectedFriendIdx === undefined) {
      return;
    }
    return this.friends[this.selectedFriendIdx].position;
  }

  @action
  addFriend(friend: Friend) {
    this.friends.push(friend);
  }

  @action
  import(friends: string) {
    let parsedFriends;
    try {
      parsedFriends = JSON.parse(friends);
    } catch (e) {
      return {
        success: false,
        errors: [new Error('invalid json')],
      };
    }
    const isValid = schemaChecker
      .validate({ $ref: 'app#/definitions/Friends' }, parsedFriends);

    if (!isValid) {
      return {
        success: false,
        errors: schemaChecker.errors,
      };
    }

    this.friends = parsedFriends;

    return {
      success: true,
    };
  }

  @action
  setSelectedFriend(index: number) {
    this.selectedFriendIdx = index;
  }
}
