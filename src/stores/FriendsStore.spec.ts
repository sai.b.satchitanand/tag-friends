import FriendsStore, { Friend } from './FriendsStore';
import { LatLng } from 'leaflet';

describe('FriendsStore:', () => {
  let store: FriendsStore;

  beforeEach(() => {
    store = new FriendsStore();
  });

  it('should import valid friends string to friends', () => {
    const sampleFriends: Friend[] = [{
      name: 'test',
      age: 20,
      photo: 'test',
      position: new LatLng(12, 12),
    }];

    const resp = store.import(JSON.stringify(sampleFriends));

    expect(store.friends).toStrictEqual(sampleFriends);
    expect(resp.success).toBe(true);
  });

  it('should return invalid json error', () => {
    const sampleFriends = 'test';

    const resp = store.import(JSON.stringify(sampleFriends));

    expect(store.friends).toStrictEqual([]);
    expect(resp.success).toBe(false);
  });

  it('should return error for missing required fields', () => {
    const sampleFriends = [{
      age: 20,
      photo: 'test',
      position: new LatLng(12, 12),
    }];

    const resp = store.import(JSON.stringify(sampleFriends));

    expect(store.friends).toStrictEqual([]);
    expect(resp.success).toBe(false);
  });
});
