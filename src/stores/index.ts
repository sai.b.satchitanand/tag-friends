import { configure } from 'mobx';
import { useStaticRendering } from 'mobx-react-lite';
import FriendsStore from './FriendsStore';

useStaticRendering(true);
configure({
  enforceActions: 'observed',
});

export default {
  FriendsStore,
};
