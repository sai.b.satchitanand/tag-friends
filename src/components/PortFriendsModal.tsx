import React, { useCallback } from 'react';
import {
  Modal,
  Button,
  Input,
  Form,
} from 'antd';
import { Observer } from 'mobx-react-lite';
import { LatLng } from 'leaflet';
import { ModalProps } from 'antd/lib/modal';
import { FormComponentProps } from 'antd/lib/form';

import { Friend } from '../../src/stores/FriendsStore';
import { useFriendsStoreContext } from '../../src/context';

interface PortFriendsModalProps extends FormComponentProps {
  modalProps: ModalProps;
  setVisible: (f: boolean) => void;
}

const sampleFriend: Friend = {
  age: 20,
  name: 'test',
  photo: 'URL',
  position: new LatLng(12, 12),
};

function PortFriendsModal({
  modalProps,
  setVisible,
  form: {
    getFieldDecorator,
    getFieldValue,
    setFields,
  },
}: PortFriendsModalProps) {
  const friendsStore = useFriendsStoreContext();
  const handleCancel = useCallback(
    () => {
      setVisible(false);
    },
    [setVisible],
  );
  const handleImport = useCallback(
    () => {
      const resp = friendsStore.import(getFieldValue('friends'));
      if (!resp.success) {
        setFields({
          friends: {
            value: getFieldValue('friends'),
            errors: resp.errors,
          },
        });

        return;
      }
      setVisible(false);
    },
    [setVisible],
  );

  return (
    <>
      <Modal
        title="Import/Export Friends"
        okText="Submit"
        onCancel={handleCancel}
        footer={[
          <Button
            key="import"
            type="primary"
            onClick={handleImport}
          >
            Import
          </Button>,
        ]}
        {...modalProps}
      >
        <Form
          className="port-form"
        >
          <pre className="pretty-json">
            {
              `Sample: \n [${JSON.stringify(sampleFriend, null, 2)}]`
            }
          </pre>
            <Observer>
              {
                () => (
                  <Form.Item>
                    {getFieldDecorator('friends', {
                      initialValue: friendsStore.friendsToString,
                      rules: [{
                        required: true,
                        message: 'Incorrect format',
                      }],
                    })(
                      <Input.TextArea
                        rows={6}
                        placeholder="Enter friends"
                      />,
                    )}
                  </Form.Item>
                )
              }
            </Observer>
        </Form>
      </Modal>
      <style jsx>{`
        .pretty-json {
          font-size: 10px;
          font-style: italic;
        }
      `}</style>
    </>
  );
}

const WrappedPortFriendsModal = Form
  .create<PortFriendsModalProps>({ name: 'portFriends' })(PortFriendsModal);

export default WrappedPortFriendsModal;
