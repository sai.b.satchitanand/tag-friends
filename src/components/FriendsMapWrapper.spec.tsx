import React from 'react';
import { render, fireEvent } from 'react-testing-library';

import FriendsMapWrapper from './FriendsMapWrapper';

const mock = jest.fn();

jest.mock('./FriendsMap', () => ({ onClick }) => (
  <span onClick={() => { onClick({ latlng: [1, 0] }); }}>map</span>
));
jest.mock('./FriendModal', () => ({ modalProps, position }) => (
  <span>{mock(modalProps.visible, position)}</span>
));

describe('FriendsMapWrapper:', () => {
  it('should set latlng in state and open modal on click of map', () => {
    const { getByText } = render(<FriendsMapWrapper />);
    expect(mock).toHaveBeenCalledWith(false, { lat: 0, lng: 0 });
    fireEvent.click(getByText('map'));
    expect(mock).toHaveBeenCalledWith(true, [1, 0]);
  });
});
