import React, { useCallback } from 'react';
import { Observer } from 'mobx-react-lite';
import { AutoComplete } from 'antd';

import { useFriendsStoreContext } from '../../src/context';
import { Friend } from '../../src/types';

const Option = AutoComplete.Option;

function renderOption(item: Friend, index) {
  const key = `${index},${item.name}`;
  return (
    <Option key={key}>
      {item.name}
    </Option>
  );
}

function FriendsSearch() {
  const friendsStore = useFriendsStoreContext();
  const handleSelect = useCallback(
    (selected: string) => {
      friendsStore
        .setSelectedFriend(parseInt(selected.split(',')[0], 10));
    },
    [],
  );

  return (
    <>
      <Observer>{() => (
        <div
          className="autocomplete-friends"
        >
          <AutoComplete
            dataSource={friendsStore.friends.map(renderOption)}
            style={{ width: 300 }}
            placeholder="search your friend"
            allowClear={true}
            onSelect={handleSelect}
            filterOption={true}
          >
          </AutoComplete>
        </div>
      )}
      </Observer>
      <style jsx>{`
        .autocomplete-friends {
          position: absolute;
          top: 10px;
          left: 50px;
          z-index: 1000;
        }
      `}</style>
    </>
  );
}

export default FriendsSearch;
