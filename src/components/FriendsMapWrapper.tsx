import React, { useCallback, useState, useMemo } from 'react';
import { LeafletMouseEvent, LatLng } from 'leaflet';

import FriendsMap from './FriendsMap';
import FriendModal, { FormData } from './FriendModal';
import PortFriendsModal from './PortFriendsModal';
import { useFriendsStoreContext } from '../../src/context';
import FriendsSearch from './FriendsSearch';

function FriendsMapWrapper() {
  const friendsStore = useFriendsStoreContext();
  const [position, setPosition] = useState<LatLng>(new LatLng(0, 0));
  const [isFriendsModalVisible, setFriendsModalVisible] = useState<boolean>(false);
  const [isPortFriendsModalVisible, setPortFriendsModalVisible] = useState<boolean>(false);

  const handleClick = useCallback(
    (e: LeafletMouseEvent) => {
      setPosition(e.latlng);
      setFriendsModalVisible(true);
    },
    [],
  );
  const handlePort = useCallback(
    () => {
      setPortFriendsModalVisible(true);
    },
    [],
  );

  const addFriend = useCallback(
    (formData: FormData) => {
      friendsStore.addFriend({
        position,
        ...formData,
      });
    },
    [position],
  );

  return (
    <>
      {/* optimize map re-render */}
      {
        useMemo(() => (
          <FriendsMap
            onClick={handleClick}
            onPort={handlePort}
          />
        ),      [])
      }
      <FriendModal
        modalProps={{
          visible: isFriendsModalVisible,
        }}
        setVisible={setFriendsModalVisible}
        position={position}
        onSubmit={addFriend}
      />
      <PortFriendsModal
        modalProps={{
          visible: isPortFriendsModalVisible,
        }}
        setVisible={setPortFriendsModalVisible}
      />
      <FriendsSearch />
    </>
  );
}

export default FriendsMapWrapper;
