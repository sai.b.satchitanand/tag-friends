import React from 'react';
import { renderToString } from 'react-dom/server';
import {
  Map,
  TileLayer,
  Marker,
  Tooltip,
 } from 'react-leaflet';
import Control from 'react-leaflet-control';
import { LeafletMouseEvent, divIcon } from 'leaflet';
import { Avatar, Button, Icon } from 'antd';
import { Observer, useObserver } from 'mobx-react-lite';

import { useFriendsStoreContext } from '../../src/context';

type Props = {
  onClick: (e: LeafletMouseEvent) => void;
  onPort: () => void;
};

function getIcon(url) {
  return divIcon({
    className: 'icon',
    html: renderToString(<Avatar size="large" src={url} />),
  });
}

function FriendsMap({ onClick, onPort }: Props) {
  const friendsStore = useFriendsStoreContext();
  const position = [52.5170365, 13.3888599];
  const zoom = 13;

  return useObserver(() => (
    <>
      <Map
        className="friends-map"
        center={friendsStore.selectedFriendPosition || position}
        zoom={zoom}
        onClick={onClick}
      >
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Control position="topleft" >
          <Button
            className="btn-port"
            shape="circle"
            onClick={onPort}
          >
            <Icon
              type="swap"
              rotate={90}
              style={{ fontSize: '16px' }}
            />
          </Button>
        </Control>
        {/* Only markers re-renders when store updates */}
        <Observer>{() => (
          <>
            {
              friendsStore.friends.map((friend, index) => (
                <Marker
                  key={index}
                  position={friend.position}
                  icon={getIcon(friend.photo)}
                >
                  <Tooltip
                    direction="right"
                    className="map-tooltip"
                  >
                    <div>
                      <span>
                        <div className="map-tooltip__name">{friend.name}</div>
                        <div><span className="map-tooltip__age-label">Age:</span>{friend.age}</div>
                      </span>
                    </div>
                  </Tooltip>
                </Marker>
              ))
            }
          </>
        )}
        </Observer>
      </Map>
      <style jsx global>{`
        .friends-map {
          height: 100vh;
        }
        .map-tooltip {
          position: relative;
          top: 15px;
          left: 30px;
        }
        .map-tooltip__name, .map-tooltip__age-label {
          font-weight: bold;
          font-size: 14px;
        }
      `}</style>
    </>
  ));
}

export default FriendsMap;
