import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';

import FriendModal from './FriendModal';

describe('FriendModal:', () => {
  let addFriendMock;
  let setVisibleMock;
  beforeEach(() => {
    addFriendMock = jest.fn();
    setVisibleMock = jest.fn();
  });

  afterEach(cleanup);

  it('should not add friend if the form is invalid', () => {
    const { getByText } = render(
      <FriendModal
        onSubmit={addFriendMock}
        modalProps={{
          visible: true,
        }}
        setVisible={setVisibleMock}
      />,
    );

    fireEvent.click(getByText('Submit'));

    expect(addFriendMock).not.toHaveBeenCalled();
  });

  it('should invoke addFriend if the form is valid and should reset form', () => {
    const { getByText, getByPlaceholderText } = render(
      <FriendModal
        onSubmit={addFriendMock}
        modalProps={{
          visible: true,
        }}
        setVisible={setVisibleMock}
      />,
    );

    const nameInput = getByPlaceholderText('Name');
    const photoInput = getByPlaceholderText('Photo: URL');

    fireEvent.change(nameInput, { target: { value: 'test' } });
    fireEvent.change(photoInput, { target: { value: 'test' } });

    fireEvent.click(getByText('Submit'));

    expect(addFriendMock).toHaveBeenCalled();
    expect(setVisibleMock).toHaveBeenCalled();
    expect(addFriendMock.mock.calls[0][0]).toMatchSnapshot();
    expect((nameInput as HTMLInputElement).value).toBe('');
  });

  it('should clear form on cancel', () => {
    const { getByText, getByPlaceholderText } = render(
      <FriendModal
        onSubmit={addFriendMock}
        modalProps={{
          visible: true,
        }}
        setVisible={setVisibleMock}
      />,
    );

    const nameInput = getByPlaceholderText('Name');
    const photoInput = getByPlaceholderText('Photo: URL');

    fireEvent.change(nameInput, { target: { value: 'test' } });
    fireEvent.change(photoInput, { target: { value: 'test' } });

    fireEvent.click(getByText('Cancel'));

    expect((nameInput as HTMLInputElement).value).toBe('');
    expect((photoInput as HTMLInputElement).value).toBe('');
  });
});
