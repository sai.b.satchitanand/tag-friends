import React, { useCallback } from 'react';
import {
  Form,
  Modal,
  Input,
  InputNumber,
  Divider,
  Avatar,
} from 'antd';
import { ModalProps } from 'antd/lib/modal';
import { FormComponentProps } from 'antd/lib/form';
import { LatLng } from 'leaflet';

export type FormData = {
  name: string,
  age: number,
  photo: string,
};

interface FriendModalProps extends FormComponentProps {
  modalProps: ModalProps;
  setVisible: (f: boolean) => void;
  position?: LatLng;
  onSubmit: (formData: FormData) => void;
}

const avatars = [
  'https://api.adorable.io/avatars/58/random1.png',
  'https://api.adorable.io/avatars/58/random2.png',
  'https://api.adorable.io/avatars/58/random3.png',
  'https://api.adorable.io/avatars/58/random4.png',
];

function FriendModal({
  modalProps,
  setVisible,
  form: {
    getFieldDecorator,
    validateFields,
    setFieldsValue,
    resetFields,
  },
  onSubmit,
}: FriendModalProps) {
  const handleAvatarSelect = useCallback(
    (url: string) => {
      setFieldsValue({
        photo: url,
      });
    },
    [],
  );
  const handleOk = useCallback(
    () => {
      validateFields((err, values) => {
        if (!err) {
          onSubmit(values as FormData);
          setVisible(false);
          resetFields();
        }
      });
    },
    [onSubmit, setVisible],
  );
  const handleCancel = useCallback(
    () => {
      resetFields();
      setVisible(false);
    },
    [setVisible],
  );

  return (
    <>
      <Modal
        title="Add friend"
        okText="Submit"
        onOk={handleOk}
        onCancel={handleCancel}
        {...modalProps}
      >
        <Form
          className="friend-form"
        >
          <Form.Item>
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please input your name!' }],
            })(
              <Input
                placeholder="Name"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('age')(
              <InputNumber
                min={1}
                placeholder="Age"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('photo', {
              rules: [{ required: true, message: 'You look awesome!' }],
            })(
              <Input
                placeholder="Photo: URL"
              />,
            )}
          </Form.Item>
          <Divider className="divider">or <br/> select from the avatars</Divider>
          <div className="avatars">
            {
              avatars.map((url, index) => (
                <span key={index} className="avatar" onClick={() => { handleAvatarSelect(url); }}>
                  <Avatar
                    src={url}
                  />
                </span>
              ))
            }
          </div>
        </Form>
        <style jsx>{`
          .avatars {
            text-align: center;
          }
          .avatar {
            cursor: pointer;
            padding-right: 10px;
          }
          :global(.friend-form .divider) {
            font-size: 10px;
            color: rgba(0, 0, 0, 0.65);
          }
        `}</style>
      </Modal>
    </>
  );
}

const WrappedFriendFormModal = Form
  .create<FriendModalProps>({ name: 'addFriend' })(FriendModal);

export default WrappedFriendFormModal;
