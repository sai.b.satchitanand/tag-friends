import React from 'react';
import { render, fireEvent, cleanup } from 'react-testing-library';

import FriendsMap from './FriendsMap';

describe('FriendsMap:', () => {
  afterEach(cleanup);

  it('should call wrapper click on map click', () => {
    const onClick = jest.fn();
    const onPort = jest.fn();
    const { container } = render(<FriendsMap onClick={onClick} onPort={onPort} />);

    fireEvent.click(container.querySelector('.friends-map'));

    expect(onClick).toHaveBeenCalled();
  });

  it('should call wrapper port on map control import/export click', () => {
    const onClick = jest.fn();
    const onPort = jest.fn();
    const { container } = render(<FriendsMap onClick={onClick} onPort={onPort} />);

    fireEvent.click(container.querySelector('.btn-port'));

    expect(onPort).toHaveBeenCalled();
  });
});
