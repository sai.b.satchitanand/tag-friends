const withTypescript = require('@zeit/next-typescript');
const withCss = require('@zeit/next-css');

if (typeof require !== 'undefined') {
  require.extensions['.css'] = file => {};
}

const compose = (...fns) => (
  fns.reduceRight((prevFn, nextFn) => (...args) => nextFn(prevFn(...args)), value => value)
);

const withDecorators = compose(
  withCss,
  withTypescript,
);

module.exports = withDecorators({
  target: process.env.ENV === 'server'? 'server' : 'serverless',
  webpack: config => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty',
    }

    return config;
  }
});
